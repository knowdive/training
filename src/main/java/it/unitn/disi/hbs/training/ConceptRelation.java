/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.hbs.training;

import com.fasterxml.jackson.databind.JsonNode;

/**
 *
 * @author enrico
 */
public class ConceptRelation {

    private Long id;
    private String type;
    private Concept source;
    private Concept target;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Concept getSource() {
        return source;
    }

    public void setSource(Concept source) {
        this.source = source;
    }

    public Concept getTarget() {
        return target;
    }

    public void setTarget(Concept target) {
        this.target = target;
    }
    
    @Override
    public String toString() {
        return "("+getType() + ") " + getTarget().getName();
    }

    public static ConceptRelation fromJson(JsonNode node) {
        ConceptRelation relation = new ConceptRelation();
        relation.setId(node.get("id").asLong());
        relation.setType(node.get("type").asText());
        relation.setSource(Concept.fromJson(node.get("source")));
        relation.setTarget(Concept.fromJson(node.get("target")));
        return relation;
    }
}
