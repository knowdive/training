package it.unitn.disi.hbs.training;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 * Hbs Webapi Training: Hello world!
 *
 */
public class App {

    //where the api are located
    public static String API = "http://192.168.213.74:80/hbs-test";

    public static void main(String[] args) {
        //create an instance of the application and start it
        new App().start();
    }

    /**
     * Gets a concept using it's id and finds a list of concepts related to
     * this one.
     */
    private void start() {
        try {
            //get a concept using it's id. 
            //'Car' concept has id=15600
            Concept sourceConcept = getConceptById(15600);
            System.out.println(sourceConcept.toString());

            //retrieve all concepts related to the previous one
            List<ConceptRelation> relatedConcepts = getRelatedConceptsBySourceId(sourceConcept.getId());

            //print all concepts related to 'Car'
            for (ConceptRelation relatedConcept : relatedConcepts) {
                System.out.println("    " + relatedConcept.toString());
            }
        } catch (IOException ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Concept getConceptById(long id) throws IOException {
        HttpGet getRequest = new HttpGet(API 
                + "/concepts/"
                + id
                + "?includeTimestamps=false"
                + "&includeRelationsCount=false");

        JsonNode node = performGet(getRequest);
        return Concept.fromJson(node);
    }

    public List<ConceptRelation> getRelatedConceptsBySourceId(long id) throws IOException {
        HttpGet getRequest = new HttpGet(API 
                + "/conceptrelations"
                + "?source=" + id
                + "&includeSource=true"
                + "&includeTarget=true"
                + "&includeTimestamps=false"
                + "&includeRelationsCount=false");

        List<ConceptRelation> relatedConcepts = new ArrayList<ConceptRelation>();

        JsonNode jsonResponse = performGet(getRequest);
        Iterator it = jsonResponse.elements();
        while (it.hasNext()) {
            JsonNode node = (JsonNode) it.next();
            ConceptRelation relation = ConceptRelation.fromJson(node);
            relatedConcepts.add(relation);
        }
        return relatedConcepts;

    }

    public JsonNode performGet(HttpGet request) throws IOException {
        DefaultHttpClient httpClient = new DefaultHttpClient();
        request.addHeader("accept", "application/json");
        HttpResponse response = httpClient.execute(request);

        if (response.getStatusLine().getStatusCode() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatusLine().getStatusCode());
        }

        BufferedReader br = new BufferedReader(
                new InputStreamReader((response.getEntity().getContent())));

        StringBuilder buffer = new StringBuilder();
        String line;

        while ((line = br.readLine()) != null) {
            buffer.append(line);
        }
        //System.out.println("Output from Server:");
        //System.out.println(buffer.toString());
        httpClient.getConnectionManager().shutdown();

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readTree(buffer.toString());
    }

}
