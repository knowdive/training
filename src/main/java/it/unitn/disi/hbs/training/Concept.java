/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.hbs.training;

import com.fasterxml.jackson.databind.JsonNode;

/**
 *
 * @author enrico
 */
public class Concept {

    private long id;
    private String name;
    private String description;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "[" + getId() + "] " + getName() + ": " + getDescription();
    }

    public static Concept fromJson(JsonNode node) {
        Concept concept = new Concept();
        concept.setId(node.get("id").asLong());
        JsonNode nameNode = node.get("name").get("en");
        if (nameNode != null) {
            concept.setName(nameNode.asText());
        }
        JsonNode descriptionNode = node.get("description").get("en");
        if (descriptionNode != null) {
            concept.setDescription(descriptionNode.asText());
        }
        return concept;
    }
}
